
### What is this repository for? ###

Android Çeviri-Ezber uygulaması Android Studio ortamında, Sqlite veritabanı kullanılarak gerçekleştirilmiştir.
-Çevirisini görmek istediğimiz kelimenin yanlış yazımı durumunda düzeltmesi yapılarak çevirisi ekranda gösterilmektedir.
-Ezber kısmında, ekrana yansıyan kelimenin ekranda görünmesi, kaç defa ekrana yansıdığı sayıya bağlıdır.
Kelimenin ekrana her gelmesi ezberlenme durumunu arttırarak ekrana gelme olasılığını azaltır.